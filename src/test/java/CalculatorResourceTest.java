import org.junit.Test;
import resources.CalculatorResource;

import static org.junit.Assert.assertEquals;

public class CalculatorResourceTest{

    @Test
    public void testCalculate(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "300 + 100";
        assertEquals("400", calculatorResource.calculate(expression));

        expression = "300 - 99";
        assertEquals("201", calculatorResource.calculate(expression));

        expression = "10 * 2";
        assertEquals("20", calculatorResource.calculate(expression));

        expression = "10 / 5";
        assertEquals("2", calculatorResource.calculate(expression));
    }

    @Test
    public void testCalculateThreeNumbers(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "300 + 100 + 500";
        assertEquals("900", calculatorResource.calculate(expression));

        expression = "300 - 99 - 1";
        assertEquals("200", calculatorResource.calculate(expression));

        expression = "10 * 2 * 2";
        assertEquals("40", calculatorResource.calculate(expression));

        expression = "10 / 5 / 2";
        assertEquals("1", calculatorResource.calculate(expression));
    }

    @Test
    public void testSum(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "300+100";
        assertEquals(400, calculatorResource.sum(expression));

        expression = "300+99";
        assertEquals(399, calculatorResource.sum(expression));
    }

    @Test
    public void testSubtraction(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "300-100";
        assertEquals(200, calculatorResource.subtraction(expression));

        expression = "300-99";
        assertEquals(201, calculatorResource.subtraction(expression));
    }

    @Test
    public void testMultiplication(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "10*2";
        assertEquals(20, calculatorResource.multiplication(expression));

        expression = "10*5";
        assertEquals(50, calculatorResource.multiplication(expression));
    }

    @Test
    public void testDivision(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "10/2";
        assertEquals(5, calculatorResource.division(expression));

        expression = "10/5";
        assertEquals(2, calculatorResource.division(expression));
    }
}
